class CreateVisits < ActiveRecord::Migration[6.0]
  def change
    create_table :visits do |t|
      t.references :link
      t.string :ip

      t.timestamps null: false
    end
  end
end
