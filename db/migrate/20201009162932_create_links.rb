class CreateLinks < ActiveRecord::Migration[6.0]
  def change
    create_table :links do |t|
      t.text :url
      t.string :token

      t.timestamps null: false
    end

    add_index :links, :token, unique: true
  end
end
