class Link < ApplicationRecord
  has_many :visits

  validates :url, format: URI::regexp(%w(http https)), presence: true, uniqueness: true
  validates :token, presence: true, uniqueness: true
end
