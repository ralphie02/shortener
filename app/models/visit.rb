class Visit < ApplicationRecord
  belongs_to :link

  validates :link_id, presence: true
  validates :ip, presence: true
end
