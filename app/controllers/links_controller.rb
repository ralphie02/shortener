class LinksController < ApplicationController
  def index
    @links = Link.all
    @link = Link.new
  end

  def create
    @link = Link.find_or_initialize_by(link_params)
    @link.token = new_token unless Link.exists? @link.id

    if @link.save
      redirect_to link_path(@link.token)
    else
      flash[:alert] = @link.errors.full_messages
      redirect_to root_path
    end
  end

  def show
    @link = Link.find_by(token: params[:token])
  end

  def external
    @link = Link.find_by(token: params[:token])
    visit = @link.visits.new(ip: request.remote_ip)

    if visit.save
      redirect_to @link.url
    else
      flash[:alert] = visit.errors.full_messages
      redirect_to link_path(@link.token)
    end
  end

  private

  def link_params
    params.require(:link).permit(:url)
  end

  def new_token
    loop do
      token = SecureRandom.urlsafe_base64(4)
      break token unless Link.find_by(token: token)
    end
  end
end
