Rails.application.routes.draw do
  root to: "links#index" # or new? can't decide...
  post '/', to: 'links#create'
  get ':token', to: 'links#external', as: :external, param: :token
  get ':token/info', to: 'links#show', as: :link, param: :token
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
